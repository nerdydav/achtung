def handle_cors(request, cors_domain):
    '''
    Will spin this out into it's own little module
    for importing. To make sure this isn't invoked willy nilly
    we'll test for OPTIONS request method.
    '''
    if request.method != 'OPTIONS':
        return
    
    print("Processing CORS")

    headers = {
        'Access-Control-Allow-Origin': cors_domain,
        'Access-Control-Allow-Methods': 'POST',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Max-Age': '300'
    }

    return ('', 204, headers)