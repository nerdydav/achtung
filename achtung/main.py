import os
from jira.client import JIRA
import sys
from .handle_cors import handle_cors

def cap_request(request):

    # Collecting environment variables
    jira_api_key = os.environ.get("JIRA_API_KEY", '')
    jira_server_url = os.environ.get("JIRA_URL", '')
    jira_user = os.environ.get("JIRA_USER", '')
    jira_project = os.environ.get("JIRA_PROJECT_KEY", '')

    cors_domain = os.environ.get("CORS_DOMAIN", '')

    ## Handle CORS

    if request.method == 'OPTIONS':
        handle_cors(request, cors_domain)
    

        
    headers = {
        'Access-Control-Allow-Origin': cors_domain,
        'Access-Control-Allow-Methods': 'POST',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Max-Age': '300',
        'Content-Type': 'text/plain'
    }
    print("json = " + str(request.get_json()))
    print("args = " + str(request.args))
    print("request = " + str(request))
    print("request = " + str(request.headers))
    request_json = request.get_json(silent=True)

    contact_name = request_json['name']
    contact_email = request_json['email']
    contact_company = request_json['company']
    contact_ph = request_json['phone']
    contact_message = request_json['message']
    contact_state = request_json['state']

    jira_options = {'server': jira_server_url}
    try:
        jira = JIRA(options=jira_options, basic_auth=(jira_user, jira_api_key))
    except:
        print("An unexpected error has occurred while authenticating to Jira", sys.exc_info())
        return ('{"response": "CRM Auth Failed"}, 500, headers')

    description_string = """\
                        Name: {}
                        Email:{}
                        Company: {}
                        Phone: {}
                        Message: {}
                        State: {}
                        """.format(contact_name, contact_email,
                        contact_company, contact_ph, contact_message,
                        contact_state)
    
    lead_data = {
        'project': {'key': jira_project},
        'summary': "New lead from website.",
        'description': description_string,
        'issuetype': 'Lead'
    }
     


    try:
        new_issue = jira.create_issue(fields=lead_data)
        return ('{"response": "Success"}', 200, headers)
    except:
        print("Problem creating lead in Jira", sys.exc_info())
        return ('{"response": "Something went wrong."}', 500, headers)