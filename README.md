# achtung

Achtung is a contact engine.
Utilising Jira (usually the CRM module) and run on google cloud functions it provides a simple and secure way to alert you when a someone completes a contact form on your website. 

## Requirements

python 3
venv 
jira

## Issues

Currently this is not working. It used to build on cloud functions but no longer does. My site no longer uses this so I'm not inclined to fix it up soon unless someone requests for me to do so, or sends over a merge. 